<?php

namespace App;

use mysqli;

/**
 * Класс подключения и работы с бд
 * Class AcademyDB
 * @package app
 */
class AcademyDB
{
    private $mysqli;

    /**
     * AcademyDB constructor.
     */
    public function __construct()
    {
        $this->mysqli = new mysqli("localhost", "root", "", "aero");
        $this->mysqli->query("SET NAMES 'utf8'");
    }

    /**
     * AcademyDB destructor.
     */
    public function __destruct()
    {
        $this->mysqli->close();
    }

    /**
     * SQL-запрос к AcademyDB на запись строки.
     * @param $full_name
     * @param $phone_number
     * @param $email
     * @param $birthday
     * @param $comment
     * @return string
     */
    public function insertIntoAcademy($full_name, $phone_number, $email, $birthday, $comment)
    {
        $insert = new Input($full_name, $phone_number, $email, $birthday, $comment);

        if ($insert->isValid()) {
            $query = "INSERT INTO academy (`name`, `phone_number`, `email`, `birthday`, `comment`) 
                VALUES " . $insert->getInputsAsRow();
            $insertRow = $this->mysqli->query($query);

            if ($insertRow) {
                return "";
            } else {
                return "Проблемы с подключением к дб";
            }
        } else {
            return $insert->getErrorMsg();
        }
    }
}
