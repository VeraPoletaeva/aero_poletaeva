<?php

namespace App;

/**
 * Class Input
 * @package app
 */
class Input
{
    private $errorMsg = "";
    private $full_name = "";
    private $phone_number = "";
    private $email = "";
    private $birthday = "";
    private $comment = "";

    /**
     * Input constructor.
     * @param $full_name
     * @param $phone_number
     * @param $email
     * @param $birthday
     * @param $comment
     */
    public function __construct($full_name, $phone_number, $email, $birthday, $comment)
    {
        $this->full_name = $this->clean($full_name);
        $this->phone_number = $this->clean($phone_number);
        $this->email = $this->clean($email);
        $this->birthday = $this->clean($birthday);
        $this->comment = $this->clean($comment);

        $this->checkCorrection();
    }

    /**
     * Экранирование, удаление лишних пробелов.
     * @param $value
     * @return string
     */
    private function clean($value)
    {
        $value = trim($value);
        $value = addslashes($value);
        $value = strip_tags($value);
        $value = htmlspecialchars($value);

        return $value;
    }

    /**
     * Проверка на правильность. Объединение всех проверок.
     */
    private function checkCorrection()
    {
        $this->checkFullName();
        $this->checkPhoneNumber();
        $this->checkEmail();
        $this->checkBirthday();
        $this->checkComment();
    }

    /**
     * Проверка имени.
     */
    private function checkFullName()
    {
        if (
            !preg_match('/^[А-ЯЁ][а-яё]+ [А-ЯЁ][а-яё]+ [А-ЯЁ][а-яё]$/u', $this->full_name)
            || mb_strlen($this->full_name) < 6
            || mb_strlen($this->full_name) > 20
        ) {
            $this->errorMsg = $this->errorMsg . "Длина имени должна быть не менее 6 и не более 20 символов, 
                разрешается использование только русских букв<br>";
        }
    }

    /**
     * Проверка телефонного номера.
     */
    private function checkPhoneNumber()
    {
        if (preg_match('/^[+]?[7]([\d]{10})$', $this->phone_number)) {
            $this->errorMsg = $this->errorMsg . "Номер телефона должен быть в формате 
                +7 и еще 10 цифр<br>";
        }
    }

    /**
     * Проверка email.
     */
    private function checkEmail()
    {
        if (
            !filter_var($this->email, FILTER_VALIDATE_EMAIL)
            || mb_strlen($this->email) < 6
            || mb_strlen($this->email) > 20
        ) {
            $this->errorMsg = $this->errorMsg . "Длина почты должна быть не менее 6 и не более 20 символов
                формат почты должен иметь вид: login@example.com<br>";
        }
    }

    /**
     * Проверка даты рождения.
     */
    private function checkBirthday()
    {
        $birthday_arr = explode('-', $this->birthday);

        if (!checkdate($birthday_arr[1], $birthday_arr[2], $birthday_arr[0])
            || $this->birthday < '1920-01-01' || $this->birthday > date('Y-m-d')
        ) {
            $this->errorMsg = $this->errorMsg . "Такой даты не существует<br>";
        }
    }

    /**
     * Проверка длины комментария.
     */
    private function checkComment()
    {
        if (mb_strlen($this->comment) < 10) {
            $this->errorMsg = $this->errorMsg . "Длина комментария должна состамлять не менее 10 символов<br>";
        }
    }

    /**
     * Возвращает true, если нет ошибок. Иначе false.
     * @return bool
     */
    public function isValid()
    {
        if (mb_strlen($this->errorMsg) > 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @return string
     */
    public function getErrorMsg()
    {
        return $this->errorMsg;
    }

    /**
     * @return string
     */
    public function getInputsAsRow()
    {
        return '("' . $this->full_name . '","' . $this->phone_number . '","' . $this->email . '","' . $this->birthday . '","' . $this->comment . '")';
    }
}
