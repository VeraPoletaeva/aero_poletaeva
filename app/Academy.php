<?php

require 'AcademyDB.php';
require 'Input.php';

spl_autoload_register();

session_start();
$_SESSION['errors'] = "";
$SECRET_KEY = "6LewZ5oUAAAAABr_l8QYF1aLMyZRWshc55cDmIl4";

//обработка капчи
$responseKey = $_POST['g-recaptcha-response'];
$userIP = $_SERVER['REMOTE_ADDR'];

$url = "https://www.google.com/recaptcha/api/siteverify?secret=$SECRET_KEY&response=$responseKey&remoteip=$userIP";
$response = file_get_contents($url);
$response = json_decode($response);
if (!($response->success)) {
    $_SESSION['errors'] = "Пройдите капчу";
    header("Location: ../public/index.php");
    return;
}

//------------------------------------------------------------------
//                 
//------------------------------------------------------------------

$myDB = new \app\AcademyDB();

$_SESSION['errors'] = $myDB->insertIntoAcademy(
    $_POST['full_name'],
    $_POST['phone_number'],
    $_POST['email'],
    $_POST['birthday'],
    $_POST['comment']
);

header("Location: ../public/index.php");
